#! /bin/sh

if [ ! "$1" == "--skip-provision" ]; then
    for HOST in host1 host2 host3; do
        echo "setting up $HOST ..."
        vagrant up --provision $HOST
        vagrant reload $HOST
    done
fi

HOST1IP=`vagrant ssh-config host1 | grep HostName | awk '{print $2}'`
HOST2IP=`vagrant ssh-config host2 | grep HostName | awk '{print $2}'`
HOST3IP=`vagrant ssh-config host3 | grep HostName | awk '{print $2}'`

for HOST in host1 host2 host3; do
    vagrant ssh $HOST -c "sudo bash -c \"cp -f /vagrant/$HOST.wg0.conf /etc/wireguard/wg0.conf\""
    vagrant ssh $HOST -c "sudo bash -c \"sed -i \\\"s/192\.168\.121\.221/$HOST1IP/\\\" /etc/wireguard/wg0.conf\""
    vagrant ssh $HOST -c "sudo bash -c \"sed -i \\\"s/192\.168\.121\.222/$HOST2IP/\\\" /etc/wireguard/wg0.conf\""
    vagrant ssh $HOST -c "sudo bash -c \"sed -i \\\"s/192\.168\.121\.223/$HOST3IP/\\\" /etc/wireguard/wg0.conf\""
    vagrant ssh $HOST -c "sudo bash -c \"wg-quick down wg0\""
    vagrant ssh $HOST -c "sudo bash -c \"wg-quick up wg0\""
done
