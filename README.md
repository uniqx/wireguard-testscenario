# wireguard-testscenario

This is a little wireguard VPN setup scenario tested on top of vagrant VMs.

You'll need to have vagrant installed to get this running.

## run

```
bash run.sh
```

## cleanup

```
vagrant destroy
```
